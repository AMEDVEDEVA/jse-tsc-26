package ru.tsc.golovina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.repository.IProjectRepository;
import ru.tsc.golovina.tm.enumerated.Status;
import ru.tsc.golovina.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.golovina.tm.model.Project;

public final class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    @Override
    public boolean existsByName(@NotNull String userId, @NotNull String name) {
        return findByName(userId, name) != null;
    }

    @NotNull
    @Override
    public Project findByName(@NotNull final String userId, @NotNull final String name) {
        return findAll(userId).stream()
                .filter(p -> p.getName().equals(name))
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);
    }

    @Nullable
    @Override
    public Project startById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Project project = findById(userId, id);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @NotNull
    @Override
    public Project startByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final Project project = findByIndex(userId, index);
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @NotNull
    @Override
    public Project startByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Project project = findByName(userId, name);
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Nullable
    @Override
    public Project finishById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Project project = findById(userId, id);
        if (project == null) return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @NotNull
    @Override
    public Project finishByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Project project = findByIndex(userId, index);
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @NotNull
    @Override
    public Project finishByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Project project = findByName(userId, name);
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Nullable
    @Override
    public Project changeStatusById(@NotNull final String userId,
                                    @NotNull final String id,
                                    @NotNull final Status status) {
        @Nullable final Project project = findById(userId, id);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @NotNull
    @Override
    public Project changeStatusByIndex(@NotNull final String userId,
                                       @NotNull final Integer index,
                                       @NotNull final Status status) {
        @NotNull final Project project = findByIndex(userId, index);
        project.setStatus(status);
        return project;
    }

    @NotNull
    @Override
    public Project changeStatusByName(@NotNull final String userId,
                                      @NotNull final String name,
                                      @NotNull final Status status) {
        @NotNull final Project project = findByName(userId, name);
        project.setStatus(status);
        return project;
    }

}
