package ru.tsc.golovina.tm.api.entity;

import org.jetbrains.annotations.NotNull;
import ru.tsc.golovina.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull Status getStatus();

    void setStatus(@NotNull Status status);

}