package ru.tsc.golovina.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.command.AbstractAuthCommand;

public class LogoffCommand extends AbstractAuthCommand {

    @NotNull
    @Override
    public String getCommand() {
        return "logoff";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "User logoff from system";
    }

    @Override
    public void execute() {
        serviceLocator.getAuthService().logout();
    }

}
