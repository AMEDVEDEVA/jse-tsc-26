package ru.tsc.golovina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.command.AbstractTaskCommand;
import ru.tsc.golovina.tm.enumerated.Role;
import ru.tsc.golovina.tm.model.Task;
import ru.tsc.golovina.tm.util.TerminalUtil;

public final class TaskShowByNameCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

    @NotNull
    @Override
    public String getCommand() {
        return "task-show-by-name";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show task by name";
    }

    @Override
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        @NotNull final Task task = serviceLocator.getTaskService().findByName(userId, name);
        showTask(task);
    }

}
